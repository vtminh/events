package com.example.domain.base


open class DataResponse<T>(
    var data: T? = null,
    var isFromCache: Boolean = false,
    var errorMessage: Throwable? = null
) {

    fun isSuccess(): Boolean {
        return data != null && errorMessage == null
    }

}