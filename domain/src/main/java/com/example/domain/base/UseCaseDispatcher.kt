package com.example.domain.base

import kotlin.coroutines.CoroutineContext

interface UseCaseDispatcher {
    fun getIOThread(): CoroutineContext
    fun getMainThread(): CoroutineContext
    fun getComputationThread(): CoroutineContext
}
