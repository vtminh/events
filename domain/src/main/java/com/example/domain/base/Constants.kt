package com.example.domain.base

object Constants {
    const val API_ENDPOINT = "https://s3-ap-southeast-2.amazonaws.com/bridj-coding-challenge/"
}