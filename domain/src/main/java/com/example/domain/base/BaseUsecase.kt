package com.example.domain.base

import kotlinx.coroutines.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn

/**
 * @author tuanminh.vu
 */
abstract class BaseUseCase<T>(var dispatcher: UseCaseDispatcher = UseCaseDispatcherImpl()) {

    var shouldUseCache = false

    abstract fun run(): Flow<T>

    fun execute(): Flow<T> {
        return this@BaseUseCase.run()
            .flowOn(dispatcher.getIOThread())
    }

}