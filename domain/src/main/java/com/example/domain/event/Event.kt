package com.example.domain.event

import com.example.domain.base.BaseDataModel
import com.google.gson.annotations.SerializedName

/**
 * @author tuanminh.vu
 */

data class Event(
    val name: String = "",
    val price: Float = 0.0F,
    @SerializedName("available_seats") val availableSeats: Int = 0,
    val date: String? = null,
    val venue: String? = null,
    var labels: List<String>? = null,
) : BaseDataModel() {
    fun isContainPlayLabel() = labels?.contains("play") ?: false
}

