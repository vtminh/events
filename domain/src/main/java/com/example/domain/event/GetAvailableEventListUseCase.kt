package com.example.domain.event

import com.example.domain.base.BaseUseCase
import com.example.domain.base.DataResponse
import com.example.domain.event.DateTimeUtils.convertToLocalDate
import kotlinx.coroutines.flow.*
import javax.inject.Inject

class GetAvailableEventListUseCase @Inject constructor(
    private val repository: EventRepository
) : BaseUseCase<DataResponse<List<Event>>>() {

    override fun run(): Flow<DataResponse<List<Event>>> {
        val localFlow = repository.getEventListFromLocal().map {
            DataResponse(it.filterAndSort())
        }
        val remote = flow { emit(repository.getEventListFromRemote()) }
            .onEach { repository.saveEventListToDb(it.events) }
            .map {
                DataResponse(it.events.filterAndSort())
            }
            .catch {
                emit(DataResponse(errorMessage = Throwable(it.message)))
            }

        return merge(localFlow, remote)
    }

    private fun List<Event>.filterAndSort(): List<Event> {
        return this.filter { it.availableSeats > 0 }
            .sortedBy {
                it.date?.convertToLocalDate()
            }
    }
}