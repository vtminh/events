package com.example.domain.event

import kotlinx.coroutines.flow.Flow

interface EventRepository {
    suspend fun getEventListFromRemote(): EventListResponse
    fun getEventListFromLocal(): Flow<List<Event>>
    fun saveEventListToDb(eventList: List<Event>): Flow<List<Long>>
}