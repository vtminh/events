package com.example.domain.event

import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.temporal.TemporalQueries.localDate
import java.util.*


object DateTimeUtils {
    fun String.convertToLocalDate(format: String = "yyyy-MM-dd'T'HH:mm:ss'Z'"): LocalDate {
        val formatter: DateTimeFormatter =
            DateTimeFormatter.ofPattern(format, Locale.ENGLISH)
        return LocalDate.parse(this, formatter)
    }

    fun LocalDate.formatUI(displayFormat: String = "dd/MM/yyyy"): String {
        val formatter = DateTimeFormatter.ofPattern(displayFormat)
        return this.format(formatter)
    }
}