package com.example.domain.event

import com.example.domain.base.BaseDataModel
import com.google.gson.annotations.SerializedName

/**
 * @author tuanminh.vu
 */
data class EventListResponse(
    @SerializedName("events") val events: List<Event>
) : BaseDataModel()

