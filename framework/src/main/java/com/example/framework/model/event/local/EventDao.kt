package com.example.framework.model.event.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
abstract class EventDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertAll(events: List<EventEntity>): List<Long>

    @Query("""SELECT * FROM event """)
    abstract fun getEventList(): Flow<List<EventEntity>>
}