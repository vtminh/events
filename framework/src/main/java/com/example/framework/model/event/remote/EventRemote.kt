package com.example.framework.model.event.remote

import com.example.domain.event.EventListResponse
import com.example.framework.model.EventService
import com.example.framework.network.NetworkUtils
import javax.inject.Inject

/**
 * @author tuaninh.vu
 */

class EventRemote @Inject constructor(private val service: EventService) {

    fun getEventList(): EventListResponse {
        return NetworkUtils.handleServerResponse(service.getEventList())
    }
}