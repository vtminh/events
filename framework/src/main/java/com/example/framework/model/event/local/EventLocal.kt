package com.example.framework.model.event.local

import com.example.domain.event.Event
import com.example.framework.mapper.EventEntityMapper.toEvent
import com.example.framework.mapper.EventEntityMapper.toEventEntity
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

/**
 * @author tuaninh.vu
 */

class EventLocal @Inject constructor(private val eventDao: EventDao) {

    fun getEventList(): Flow<List<Event>> {
        return eventDao.getEventList().map {
            it.map { it.toEvent() }
        }
    }

    fun saveEventList(events: List<Event>): Flow<List<Long>> {
        return flow {
            val insertNewList = eventDao.insertAll(events.map { it.toEventEntity() })
            emit(insertNewList)
        }
    }
}