package com.example.framework.model

import com.example.domain.event.EventListResponse
import retrofit2.Call
import retrofit2.http.GET

/**
 * @author tuanminh.vu
 */
interface EventService {

    @GET("events.json")
    fun getEventList(): Call<EventListResponse>
}