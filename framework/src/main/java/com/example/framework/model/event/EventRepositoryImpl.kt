package com.example.framework.model.event

import com.example.domain.event.Event
import com.example.domain.event.EventListResponse
import com.example.domain.event.EventRepository
import com.example.framework.model.event.local.EventLocal
import com.example.framework.model.event.remote.EventRemote
import kotlinx.coroutines.flow.Flow

/**
 * @author tuanminh.vu
 */
class EventRepositoryImpl(
    private val remote: EventRemote,
    private val local: EventLocal
) : EventRepository {

    override suspend fun getEventListFromRemote(): EventListResponse {
        return remote.getEventList()
    }

    override fun getEventListFromLocal(): Flow<List<Event>> {
        return local.getEventList()
    }

    override fun saveEventListToDb(eventList: List<Event>): Flow<List<Long>> {
        return local.saveEventList(eventList)
    }

}