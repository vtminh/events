package com.example.framework.model.event.local

import androidx.room.*
import com.example.domain.base.BaseDataModel

@Entity(
    tableName = "event"
)
class EventEntity(
    @PrimaryKey @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "price") val price: Float = 0.0f,
    @ColumnInfo(name = "available_seats") val availableSeats: Int = 0,
    @ColumnInfo(name = "date") val date: String? = null,
    @ColumnInfo(name = "venue") val venue: String? = null,
    @ColumnInfo(name = "labels") val labels: List<String>? = null
) : BaseDataModel()