package com.example.framework.module

import com.example.domain.base.Constants.API_ENDPOINT
import com.example.domain.di.ApplicationScope
import com.example.framework.model.EventService
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named

/**
 * @author tuanminh.vu
 */

@Module
class NetworkModule {

    companion object {
        private const val SERVICE_NAME = "AmzService"
        private const val REQUEST_TIME_OUT = 45L
    }

    @Provides
    @ApplicationScope
    @Named(SERVICE_NAME)
    fun provideOKHttpClient(): OkHttpClient {
        val builder = OkHttpClient.Builder()
            .retryOnConnectionFailure(true)
            .readTimeout(REQUEST_TIME_OUT, TimeUnit.SECONDS)
            .writeTimeout(REQUEST_TIME_OUT, TimeUnit.SECONDS)
            .addInterceptor { chain ->
                chain.proceed(chain.request())
            }

        return builder.build()
    }

    @Provides
    @ApplicationScope
    @Named(SERVICE_NAME)
    fun provideRetrofit(
        @Named(SERVICE_NAME) okHttpClient: OkHttpClient,
        gson: Gson
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(API_ENDPOINT)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    @Provides
    @ApplicationScope
    fun provideUserService(@Named(SERVICE_NAME) retrofit: Retrofit): EventService {
        return retrofit.create(EventService::class.java)
    }

    @Provides
    @ApplicationScope
    fun provideGson(): Gson {
        return GsonBuilder().setLenient()
            .create()
    }
}