package com.example.framework.module

import com.example.domain.event.EventRepository
import com.example.framework.model.event.EventRepositoryImpl
import com.example.framework.model.event.remote.EventRemote
import com.example.domain.di.ApplicationScope
import com.example.framework.model.event.local.EventLocal
import dagger.Module
import dagger.Provides

/**
 * @author tuanminh.vu
 */
@Module
open class RepositoryModule {

    @ApplicationScope
    @Provides
    fun provideEventRepository(remote: EventRemote, local: EventLocal): EventRepository {
        return EventRepositoryImpl(remote, local)
    }

}