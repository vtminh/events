package com.example.framework.module

import android.app.Application
import androidx.room.Room
import com.example.domain.di.ApplicationScope
import com.example.framework.db.EventDatabase
import com.example.framework.model.event.local.EventDao
import dagger.Module
import dagger.Provides

/**
 * @author tuanminh.vu
 */
@Module
open class DatabaseModule {

    @ApplicationScope
    @Provides
    fun provideEventDao(eventDatabase: EventDatabase): EventDao {
        return eventDatabase.eventDao()
    }

    @ApplicationScope
    @Provides
    fun provideEventDatabase(app: Application): EventDatabase {
        return Room.databaseBuilder(app, EventDatabase::class.java, "event.db")
            // This is not recommended for normal apps, but the goal of this sample isn't to
            // showcase all of Room.
            .fallbackToDestructiveMigration()
            .build()
    }

}