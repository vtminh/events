package com.example.framework.mapper

import com.example.domain.event.Event
import com.example.framework.model.event.local.EventEntity

object EventEntityMapper {
    fun Event.toEventEntity(): EventEntity {
        return EventEntity(name, price, availableSeats, date, venue, labels)
    }

    fun EventEntity.toEvent(): Event {
        return Event(name, price, availableSeats, date, venue, labels)
    }
}