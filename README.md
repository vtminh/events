# Event app
#### Coding Challenge from Sharesource

![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)

## Features
- Get event list from https://s3-ap-southeast-2.amazonaws.com/bridj-coding-challenge/events.json
- Filter and sort data list
- Support offline mode

## Tech
- MVVM with Clean architecture, in which, Domain layer is pure Kotlin
- Room for database
- Jetpack Compose for View layer
- Dagger 2 for DI & module structure
- Coroutine for Threading 
- Language: Kotlin

## Testing
All of modules, classes are designed to eligible for Unit Test

## Installation
Get apk file in: /app/build/intermediates/apk/debug/app-debug.apk

## License
Vu Tuan Minh
Email: vtminh1805@gmail.com
