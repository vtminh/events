package com.example.events.ui.home

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.example.domain.event.DateTimeUtils.convertToLocalDate
import com.example.domain.event.DateTimeUtils.formatUI
import com.example.domain.event.Event
import com.example.events.ui.theme.Blue300
import com.example.events.ui.theme.Gray10
import com.example.events.ui.theme.Red300
import com.example.events.ui.theme.Yellow800

@Composable
fun EventItemView(
    event: Event,
    modifier: Modifier = Modifier
) {
    Column(
        modifier = modifier
            .fillMaxWidth()
            .background(Gray10)
            .padding(20.dp)
    ) {
        Text(
            event.name,
            style = MaterialTheme.typography.h1,
            color = MaterialTheme.colors.primary,
            textAlign = TextAlign.Center
        )
        Text(
            event.date?.convertToLocalDate()?.formatUI().orEmpty(),
            style = MaterialTheme.typography.caption,
            color = Blue300,
            textAlign = TextAlign.Center
        )

        Text(
            "$" + event.price.toString(),
            style = MaterialTheme.typography.caption,
            color = Red300,
            textAlign = TextAlign.Center
        )
        Text(
            event.venue.orEmpty() + " - " + event.availableSeats.toString() + " seats",
            style = MaterialTheme.typography.subtitle1,
            textAlign = TextAlign.Center
        )

        Text(
            event.labels.toString(),
            style = MaterialTheme.typography.subtitle1,
            textAlign = TextAlign.Center
        )
    }
}
