package com.example.events.ui.home

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.example.events.R
import com.google.accompanist.insets.statusBarsHeight
import com.google.accompanist.insets.systemBarsPadding
import com.google.accompanist.pager.ExperimentalPagerApi

@Composable
fun Home(
    viewModel: EventViewModel
) {
    Surface(Modifier.fillMaxSize()) {
        HomeContent(
            viewModel,
            modifier = Modifier.fillMaxSize()
        )
    }
}

@Composable
fun HomeAppBar(
    isFiltered: MutableState<Boolean>,
    backgroundColor: Color,
    modifier: Modifier = Modifier
) {
    TopAppBar(
        title = {
            Row {
                Image(
                    painter = painterResource(R.drawable.ic_logo),
                    contentDescription = null,
                    modifier = Modifier.align(Alignment.CenterVertically)
                )
                Text(
                    text = stringResource(R.string.app_name),
                    modifier = Modifier
                        .padding(start = 4.dp)
                        .heightIn(max = 24.dp)
                        .align(Alignment.CenterVertically)
                        .weight(1f)
                )
                IconButton(
                    modifier = Modifier.align(Alignment.CenterVertically),
                    onClick = {
                        isFiltered.value = !isFiltered.value
                    }
                ) {
                    Icon(
                        if (isFiltered.value) Icons.Default.AllOut else Icons.Default.PlayCircleFilled,
                        contentDescription = "",
                        modifier = Modifier
                            .padding(15.dp)
                            .size(24.dp)
                    )
                }
            }
        },
        backgroundColor = backgroundColor,
        modifier = modifier
    )
}

@OptIn(ExperimentalPagerApi::class)
@Composable
fun HomeContent(
    viewModel: EventViewModel,
    modifier: Modifier = Modifier
) {
    val isFiltered = remember { mutableStateOf(false) }

    Column(
        modifier = modifier
            .systemBarsPadding(top = false, bottom = false)
            .fillMaxWidth()
    ) {
        val appBarColor = MaterialTheme.colors.surface.copy(alpha = 0.87f)
        Spacer(
            Modifier
                .background(appBarColor)
                .fillMaxWidth()
                .statusBarsHeight()
        )
        HomeAppBar(
            isFiltered,
            backgroundColor = appBarColor,
            modifier = Modifier.fillMaxWidth()
        )
        EventPage(
            viewModel,
            isFiltered,
            Modifier.fillMaxWidth().fillMaxHeight()
        )
    }
}