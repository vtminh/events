package com.example.events.ui

import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.example.events.MyApplication
import com.example.events.ui.home.EventViewModel
import com.example.events.ui.home.Home
import com.example.events.util.ViewModelUtils

@Composable
fun EventApp(
    appState: EventsAppState = rememberEventsAppState()
) {
    (LocalContext.current as? MainActivity).run {
        NavHost(
            navController = appState.navController,
            startDestination = Screen.Home.route
        ) {
            composable(Screen.Home.route) { backStackEntry ->

                val component = (this@run?.application as MyApplication).getAppComponent()
                val viewModel: EventViewModel = ViewModelUtils.daggerViewModel {
                    component.getEventViewModel()
                }
                Home(viewModel)
            }
        }
    }

}