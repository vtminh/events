package com.example.events.ui.home

import android.widget.Toast
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState

@Composable
fun EventPage(
    viewModel: EventViewModel,
    isFiltered: MutableState<Boolean>,
    modifier: Modifier = Modifier,
) {
    val errorState by viewModel.errorLiveData.collectAsState()
    errorState?.let {
        Toast.makeText(LocalContext.current, it.message, Toast.LENGTH_LONG).show()
    }
    val loadingState by viewModel.loadingLiveData.collectAsState()
    if (loadingState) {
        Box(
            contentAlignment = Alignment.Center,
            modifier = Modifier.fillMaxSize()
        ) {
            CircularProgressIndicator()
        }
        return
    }

    val isRefreshing by viewModel.isRefreshing.collectAsState()

    SwipeRefresh(
        state = rememberSwipeRefreshState(isRefreshing),
        onRefresh = { viewModel.refresh() },
    ) {
        Column(modifier) {
            val liveData by viewModel.baseLiveData.collectAsState()
            liveData?.data?.takeIf { !it.isNullOrEmpty() }?.let {
                Spacer(Modifier.height(8.dp))
                LazyColumn(verticalArrangement = Arrangement.spacedBy(20.dp)) {
                    items(items = if (isFiltered.value) it.filter {
                        it.isContainPlayLabel()
                    } else it, itemContent = { event ->
                        EventItemView(
                            event = event,
                            modifier = Modifier
                                .fillMaxSize()
                                .weight(1f)
                        )
                    })
                }
            }
        }
    }
}