
package com.example.events.ui.theme

import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable

@Composable
fun EventsTheme(
    content: @Composable () -> Unit
) {
    MaterialTheme(
        colors = EventsColors,
        typography = EventsTypography,
        shapes = EventsShapes,
        content = content
    )
}
