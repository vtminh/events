package com.example.events.ui.home

import com.example.domain.event.Event
import com.example.domain.event.GetAvailableEventListUseCase
import com.example.events.base.BaseViewModel
import kotlinx.coroutines.flow.MutableStateFlow

class EventViewModel(
    private val getAvailableEventListUseCase: GetAvailableEventListUseCase
) : BaseViewModel<List<Event>>() {

    val isRefreshing = MutableStateFlow(false)

    init {
        loadData(getAvailableEventListUseCase)
    }

    fun refresh() {
        loadData(getAvailableEventListUseCase)
    }
}