
package com.example.events.ui.theme

import androidx.compose.material.darkColors
import androidx.compose.ui.graphics.Color

val Yellow800 = Color(0xFFF29F05)
val Red300 = Color(0xFFEA6D7E)
val Blue300 = Color(0xFFD7EDFF)

val Gray10 = Color(0x1AE6E4DF)

val EventsColors = darkColors(
    primary = Yellow800,
    onPrimary = Color.Black,
    primaryVariant = Yellow800,
    secondary = Yellow800,
    onSecondary = Color.Black,
    error = Red300,
    onError = Color.Black
)
