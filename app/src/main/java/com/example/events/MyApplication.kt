
package com.example.events

import android.app.Application
import com.example.framework.network.NetworkStateHandler
import com.example.events.di.components.AppComponent
import com.example.events.di.components.DaggerAppComponent

class MyApplication : Application() {
    private var appComponent: AppComponent? = null

    override fun onCreate() {
        super.onCreate()
        NetworkStateHandler.initialize(this)
        getAppComponent().inject(this)
    }

    fun getAppComponent(): AppComponent {
        if (appComponent == null) {
            appComponent = DaggerAppComponent
                .builder()
                .application(this)
                .build()
        }
        return appComponent!!
    }
}
