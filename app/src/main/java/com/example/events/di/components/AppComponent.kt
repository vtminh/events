package com.example.events.di.components

import android.app.Application
import com.example.domain.di.ApplicationScope
import com.example.framework.module.DatabaseModule
import com.example.framework.module.NetworkModule
import com.example.framework.module.RepositoryModule
import com.example.events.MyApplication
import com.example.events.di.modules.ViewModelModule
import com.example.events.ui.home.EventViewModel
import dagger.BindsInstance
import dagger.Component

/**
 * @author tuanminh.vu
 */
@ApplicationScope
@Component(
    modules = [
        NetworkModule::class,
        ViewModelModule::class,
        RepositoryModule::class,
        DatabaseModule::class
    ]
)
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(application: MyApplication)
    fun getEventViewModel(): EventViewModel

}