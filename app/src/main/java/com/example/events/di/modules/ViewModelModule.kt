package com.example.events.di.modules

import com.example.domain.di.ApplicationScope
import com.example.domain.event.GetAvailableEventListUseCase
import com.example.events.ui.home.EventViewModel
import dagger.Module
import dagger.Provides

@Module
class ViewModelModule {
    @Provides
    @ApplicationScope
    fun provideEventViewModel(
        getAvailableEventListUseCase: GetAvailableEventListUseCase
    ): EventViewModel =
        EventViewModel(getAvailableEventListUseCase)
}